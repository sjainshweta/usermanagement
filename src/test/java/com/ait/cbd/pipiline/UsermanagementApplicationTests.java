package com.ait.cbd.pipiline;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.ait.cbd.pipiline.controller.UserController;
import com.ait.cbd.pipiline.entity.User;
import com.ait.cbd.pipiline.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = UserController.class)
class UsermanagementApplicationTests {
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private UserRepository userRepository;

	User user;

	@BeforeEach
	public void setUp() {
		user = new User(1L, RandomStringUtils.random(15), RandomStringUtils.random(15),
				RandomStringUtils.random(15) + "@gmail.com");
		List<User> users = Arrays.asList(user);

		Mockito.when(userRepository.findAll()).thenReturn(users);
		Mockito.when(userRepository.findByEmail(user.getEmail())).thenReturn(users);
		Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
		Mockito.when(userRepository.save(user)).thenReturn(user);
		Mockito.doNothing().when(userRepository).delete(user);
	}

	@Test
	void whenGetAllUsers_thenReturns200() throws Exception {

		mockMvc.perform(get("/users").contentType("application/json")).andExpect(status().isOk());
	}

	@Test
	void whenCreateUserWithValidInput_thenReturns201() throws Exception {
		mockMvc.perform(
				post("/users", 42L).contentType("application/json").content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isCreated());
	}

	@Test
	void whenGetUserByEmail_thenReturns200() throws Exception {
		mockMvc.perform(get("/users/email/" + user.getEmail()).contentType("application/json"))
				.andExpect(status().isOk());
	}

	@Test
	void whenUpdateUserById_thenReturns200() throws Exception {
		user.setEmail("test");
		mockMvc.perform(put("/users/1").contentType("application/json").content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk());
	}

	@Test
	void whenDeleteUserById_thenReturns200() throws Exception {
		mockMvc.perform(delete("/users/1").contentType("application/json")).andExpect(status().isOk());
	}
}