package com.ait.cbd.pipiline.exception;

public class UserIdMismatchException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserIdMismatchException() {
		super("No User Found for a given ID !!!");
	}
}
