package com.ait.cbd.pipiline.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.ait.cbd.pipiline.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {
	List<User> findByEmail(String email);
}