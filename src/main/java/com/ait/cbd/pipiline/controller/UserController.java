package com.ait.cbd.pipiline.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.ait.cbd.pipiline.entity.User;
import com.ait.cbd.pipiline.exception.UserIdMismatchException;
import com.ait.cbd.pipiline.exception.UserNotFoundException;
import com.ait.cbd.pipiline.repository.UserRepository;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@GetMapping
	public Iterable<User> findAll() {
		return userRepository.findAll();
	}

	@GetMapping("/email/{email}")
	public List<User> findByEmail(@PathVariable String email) {
		return userRepository.findByEmail(email);
	}

	@GetMapping("/{id}")
	public User findOne(@PathVariable Long id) {
		return userRepository.findById(id).orElseThrow(UserNotFoundException::new);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public User create(@RequestBody User user) {
		return userRepository.save(user);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		userRepository.findById(id).orElseThrow(UserNotFoundException::new);
		userRepository.deleteById(id);
	}

	@PutMapping("/{id}")
	public User updateBook(@RequestBody User user, @PathVariable Long id) {
		if (user.getId() != id) {
			throw new UserIdMismatchException();
		}
		userRepository.findById(id).orElseThrow(UserNotFoundException::new);
		return userRepository.save(user);
	}
}